# dimstech-dimmaexploration

A modpack for use by my siblings and their friends.

# Mod List

## Dependencies/Frameworks

* [Aroma1997Core](https://minecraft.curseforge.com/projects/aroma1997core) - `Aroma1997Core-1.12.2-2.0.0.2.jar`
* [BdLib](https://minecraft.curseforge.com/projects/bdlib) - `bdlib-1.14.3.12-mc1.12.2.jar`
* [Bookshelf](https://minecraft.curseforge.com/projects/bookshelf) - `Bookshelf-1.12.2-2.3.577.jar`
* [brandon's Core](https://minecraft.curseforge.com/projects/brandons-core) - `BrandonsCore-1.12.2-2.4.10.198-universal.jar`
* [CodeChicken Lib 1.8+](https://minecraft.curseforge.com/projects/codechicken-lib-1-8) - `CodeChickenLib-1.12.2-3.2.2.353-universal.jar`
* [EnderCore](https://minecraft.curseforge.com/projects/endercore) - `EnderCore-1.12.2-0.5.57.jar`
* [Farseek](https://minecraft.curseforge.com/projects/farseek) - `Farseek-1.12-2.3.1.jar`
* [Shadowfacts' Forgelin](https://minecraft.curseforge.com/projects/shadowfacts-forgelin) - `Forgelin-1.8.2.jar`
* [Mantle](https://minecraft.curseforge.com/projects/mantle) - `Mantle-1.12-1.3.3.49.jar`
* [McJtyLib](https://minecraft.curseforge.com/projects/mcjtylib) - `mcjtylib-1.12-3.1.1.jar`
* [McdoodleCore](https://micdoodle8.com/mods/galacticraft/downloads) - `MicdoodleCore-1.12.2-4.0.2.210.jar`
* [Numina](https://minecraft.curseforge.com/projects/numina) - `Numina-1.12.2-1.0.36.jar`
* [OreLib](https://minecraft.curseforge.com/projects/orelib) - `OreLib-1.12.2-3.5.2.2.jar`
* [p455w0rd's Library](https://minecraft.curseforge.com/projects/p455w0rds-library) - `p455w0rdslib-1.12.2-2.1.44.jar`
* [Tesla Core Lib](https://minecraft.curseforge.com/projects/tesla-core-lib) - `tesla-core-lib-1.12.2-1.0.15.jar`
* [ZeroCore](https://minecraft.curseforge.com/projects/zerocore) - `zerocore-1.12.2-0.1.2.8.jar`

## Standalones

* [AromaBackup](https://minecraft.curseforge.com/projects/aromabackup) - `WirelessCraftingTerminal-1.12.2-3.11.88.jar`
* [Applied Energistics 2](https://ae-mod.info/) - `appliedenergistics2-rv6-stable-6.jar`
  * [AE2 Wireless Terminal Library](https://minecraft.curseforge.com/projects/ae2-wireless-terminal-library) - `WirelessCraftingTerminal-1.12.2-3.11.88.jar`
  * [AE2 Stuff](https://minecraft.curseforge.com/projects/ae2-stuff) - `ae2stuff-0.7.0.4-mc1.12.2.jar`
  * [ExtraCells](https://minecraft.curseforge.com/projects/extracells2) - `ExtraCells-1.12.2-2.6.2a.jar`
* [AtomicStryker's Battle Towers](https://minecraft.curseforge.com/projects/atomicstrykers-battle-towers) - `BattleTowers-1.12.2.jar`
* [BiblioCraft](https://www.bibliocraftmod.com/) - `BiblioCraft[v2.4.5][MC1.12.2].jar`
* [Biomes O' Plenty](https://minecraft.curseforge.com/projects/biomes-o-plenty) - `BiomesOPlenty-1.12.2-7.0.1.2419-universal.jar`
* [Buildcraft](https://minecraft.curseforge.com/projects/buildcraft) - `buildcraft-all-7.99.24.1.jar`
* [Carry On](https://minecraft.curseforge.com/projects/carry-on) - `CarryOn+MC1.12.2+v1.12.1.jar`
* [Chicken Chunks 1.8+](https://minecraft.curseforge.com/projects/chicken-chunks-1-8) - `ChickenChunks-1.12.2-2.4.1.73-universal.jar`
* [Chickens](https://minecraft.curseforge.com/projects/chickens) - `chickens-6.0.4.jar`
* [Redstone Flux](https://minecraft.curseforge.com/projects/redstone-flux) - `RedstoneFlux-1.12-2.1.0.6-univeral.jar`
  * [CoFH Core](https://minecraft.curseforge.com/projects/cofhcore) - `CoFHCore-1.12.2-4.6.2.25-universal.jar`
  * [CoFH World](https://minecraft.curseforge.com/projects/cofh-world) - `CoFHWorld-1.12.2-1.3.0.6-universal.jar`
  * [Thermal Foundation](https://minecraft.curseforge.com/projects/thermal-foundation) - `ThermalFoundation-1.12.2-2.6.2.26-universal.jar`
  * [Thermal Expansion](https://minecraft.curseforge.com/projects/thermal-expansion) - `ThermalExpansion-1.12.2-5.5.3.41-universal.jar`
  * [Thermal Dynamics](https://minecraft.curseforge.com/projects/thermal-expansion) - `ThermalDynamics-1.12.2-2.5.4.18-universal.jar`
* [Dark Utilities](https://minecraft.curseforge.com/projects/dark-utilities) - `DarkUtils-1.12.2-1.8.223.jar`
* [Draconic Evolution](https://minecraft.curseforge.com/projects/draconic-evolution) - `Draconic-Evolution-1.12.2-2.3.21.342-universal.jar`
* [Dynamic Surroundings](https://minecraft.curseforge.com/projects/dynamic-surroundings) - `DynamicSurroundings-1.12.2-3.5.4.3.jar`
* [Enchanting Plus](https://minecraft.curseforge.com/projects/enchanting-plus) - `EnchantingPlus-1.12.2-5.0.176.jar`
* [Ender IO](https://minecraft.curseforge.com/projects/ender-io) - `EnderIO-1.12.2-5.0.43.jar`
* [Ender Storage 1.8+](https://minecraft.curseforge.com/projects/ender-storage-1-8) - `EnderStorage-1.12.2-2.4.5.135-universal.jar`
* [Energy Converters](https://minecraft.curseforge.com/projects/energy-converters) - `energyconverters_1.12.2-1.2.1.11.jar`
* [Extra Utilities](https://minecraft.curseforge.com/projects/extra-utilities) - `extrautils2-1.12-1.9.9.jar`
* [Extreme Reactors](https://minecraft.curseforge.com/projects/extreme-reactors) - `ExtremeReactors-1.12.2-0.4.5.65.jar`
* [Fullscreen Windowed (Borderless) for Minecraft](https://minecraft.curseforge.com/projects/fullscreen-windowed-borderless-for-minecraft) - `FullscreenWindowed-1.12-1.6.0.jar`
* [FoamFix for Minecraft](https://minecraft.curseforge.com/projects/foamfix-for-minecraft) - `foamfix-0.10.5-1.12.2.jar`
* [Galacticraft Core](https://micdoodle8.com/mods/galacticraft/downloads) - `GalacticraftCore-1.12.2-4.0.2.210.jar`
  * [Galacticraft Planets](https://micdoodle8.com/mods/galacticraft/downloads) - `Galacticraft-Planets-1.12.2-4.0.2.210.jar`
* [GraveStone](https://minecraft.curseforge.com/projects/gravestone-mod) - `gravestone-1.10.1.jar`
* [Horizontal Glass Panes](https://minecraft.curseforge.com/projects/horizontal-glass-panes) - `hgp-1.0.jar`
* [Hwyla](https://minecraft.curseforge.com/projects/hwyla) - `Hwyla-1.8.26-B41_1.12.2.jar`
* [In Control!](https://minecraft.curseforge.com/projects/in-control) - `incontrol-1.12-3.9.6.jar`
* [Industrial Foregoing](https://minecraft.curseforge.com/projects/industrial-foregoing) - `industrialforegoing-1.12.2-1.12.11-235.jar`
* [Inventory Tweaks](https://minecraft.curseforge.com/projects/inventory-tweaks) - `InventoryTweaks-1.63.jar`
* [AtomicStryker's Infernal Mobs](https://minecraft.curseforge.com/projects/atomicstrykers-infernal-mobs) - `InfernalMobs-1.12.2.jar`
* [Inventory Pets](https://minecraft.curseforge.com/projects/inventory-pets) - `inventorypets-1.12-2.0.1.jar`
* [Just Enough Items (JEI)](https://minecraft.curseforge.com/projects/jei) - `jei_1.12.2-4.15.0.268.jar`
* [JourneyMap](https://minecraft.curseforge.com/projects/journeymap) - `journeymap-1.12.2-5.5.4.jar`
* [Mob Grinding Utils](https://minecraft.curseforge.com/projects/mob-grinding-utils) - `MobGrindingUtils-0.3.13.jar`
* [Modular Powersuits](https://minecraft.curseforge.com/projects/modular-powersuits) - `ModularPowersuits-1.12.2-1.0.39.jar`
* [Multi Mine](https://minecraft.curseforge.com/projects/multi-mine) - `MultiMine-1.12.1.jar`
* [OptiFine](https://optifine.net/downloads) - `OptiFine_1.12.2_HD_U_E3_MOD.jar`
* [Ore Excavation](https://minecraft.curseforge.com/projects/ore-excavation) - `OreExcavation-1.4.140.jar`
* [Railcraft](https://minecraft.curseforge.com/projects/railcraft) - `railcraft-12.0.0.jar`
* [RFTools](https://minecraft.curseforge.com/projects/rftools) - `rftools-1.12-7.61.jar`
  * [RFTools Dimensions](https://minecraft.curseforge.com/projects/rftools-dimensions) - `rftoolsdim-1.12-5.64.jar`
* [Roost](https://minecraft.curseforge.com/projects/roost) - `roost-1.12.2-2.0.10.jar`
* [Ruins (Structure Spawning System)](https://minecraft.curseforge.com/projects/ruins-structure-spawning-system) - `Ruins-1.12.2.jar`
* [Simply Jetpacks 2](https://minecraft.curseforge.com/projects/simply-jetpacks-2) - `SimplyJetpacks2-1.12.2-2.2.11.64.jar`
* [Serene Seasons](https://minecraft.curseforge.com/projects/serene-seasons) - `SereneSeasons-1.12.2-1.2.15-universal.jar`
* [SpatialCompat](https://minecraft.curseforge.com/projects/spatialcompat) - `spatialservermod-1.3.jar`
* [Streams](https://minecraft.curseforge.com/projects/streams) - `Streams-1.12-0.4.4.jar`
* [Surge](https://minecraft.curseforge.com/projects/surge) - `Surge-1.12.2-2.0.77.jar`
* [Tick Dynamic](https://minecraft.curseforge.com/projects/tick-dynamic) - `TickDynamic-1.12.2-1.0.1.jar`
* [Tinkers' Construct](https://minecraft.curseforge.com/projects/tinkers-construct) - `TConstruct-1.12.2-2.12.0.135.jar`
* [Twilight Forest](https://minecraft.curseforge.com/projects/the-twilight-forest) - `twilightforest-1.12.2-3.8.689-universal.jar`
* [Unloader](https://minecraft.curseforge.com/projects/unloader) - `unloader-1.2.0.jar`
* [Useful Interior](https://minecraft.curseforge.com/projects/useful-interior) - `Useful+Interior+-+0.13+[1.12.2].jar`
* [Valkyrien Warfare](https://minecraft.curseforge.com/projects/valkyrien-warfare) - `Valkyrien+Warfare+0.9_prerelease_5+[Optimization+Update].jar`
* [VanillaFix](https://minecraft.curseforge.com/projects/vanillafix) - `VanillaFix-1.0.10-99.jar`